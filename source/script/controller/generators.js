"use strict";

const {
    ipcRenderer
} = require('electron');

const {
    dialog
} = require('electron').remote;

const fs = require('fs');

const lookup = require("../net/lifestoned");

var region = null;

module.exports = exports = {
    init: () => {
        loadDataFile('region.json', 'json').then((data) => {
            region = data;
            var dd = $('#generators');

            dd.children().remove();
            dd.append($('<li value="0">None</li>'));

            $.each(region.encounters, function(key, val) {
                var item = $('<li></li>')
                    .text(val.desc || val.key)
                    .data('key', val.key)
                    .click(function(evt) {
                        $('#generators .active').toggleClass('active');
                        $(this).toggleClass('active');
                        updateGenList($(this).data('key'));
                    });
                dd.append(item);
            });
        });

        $('#groupName').change(function (evt) {
            var $spawns = $('#spawns');
            var selected = $spawns.val();
            var text = $(this).val();
            $spawns.children(`[value='${selected}']`).text(text);
            //map.getRegion().encounters[selected].desc = text;
        });
    },

    activate: () => {
        //map.init(lookup.getName, lookup.getGeneratorList);
    },

    deactivate: () => {

    },

    saveAs: () => {
        regionSave();
    }
};

function updateGenList(key) {
    key = parseInt(key);
    var enc = region.encounters[key];
    console.log(key, enc);

    $('#groupName').val(enc.desc);
    enc.value.forEach((v, k) => $(`#gen_${k.toString(16)}`).val(v));
}

function regionSave() {
    dialog.showSaveDialog(
        {
            filters : [
                { name : 'JSON Files', extensions: [ 'json' ] },
                { name : 'All Files', extensions: [ '*' ] }
            ],
        },
        function(file) {
            if (file) {
                fs.writeFile(file, JSON.stringify(map.getRegion()));
            }
        }
    );
}