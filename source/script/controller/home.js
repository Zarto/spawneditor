"use strict";

const {
    ipcRenderer
} = require('electron');

const {
    dialog
} = require('electron').remote;

const fs = require('fs');

const map = require("../ux/map");

//const lookup = require("../model/sheets");
const lookup = require("../net/lifestoned");


module.exports = exports = {
    init: () => {
        //sheets.load('1ar4-UaV-E9TV63LEH70RW49iKvHmwlvOqYDbFuvUCuk', 'NEED API KEY');
        loadDataFile('region.json', 'json').then((data) => {
            map.setRegion(data);
        });
        loadDataFile('encounters.json', 'json')
            .then((data) => {
                map.setEncounters(data);
            });

        $('#spawns').on("change", function() {
            var val = parseInt(this.value);
            $('#groupName').val(map.getRegion().encounters[val].desc);
        });

        $('#groupName').change(function (evt) {
            var $spawns = $('#spawns');
            var selected = $spawns.val();
            var text = $(this).val();
            $spawns.children(`[value='${selected}']`).text(text);
            map.getRegion().encounters[selected].desc = text;
        });
    },

    activate: () => {
        map.init(lookup.getName, lookup.getGeneratorList);
    },

    deactivate: () => {

    },

    saveAs: () => {
        regionSave();
    }
};

function regionSave() {
    dialog.showSaveDialog(
        {
            filters : [
                { name : 'JSON Files', extensions: [ 'json' ] },
                { name : 'All Files', extensions: [ '*' ] }
            ],
        },
        function(file) {
            if (file) {
                fs.writeFile(file, JSON.stringify(map.getRegion()));
            }
        }
    );
}