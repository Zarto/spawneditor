var content;
var context;
var points = new Array();
var dPoints = new Array();
var highlightedPointName = "";
var landblockPointName = "";
var highlightedDynPoint = -1;
var landblockDynPoint = -1;
var gridCount = 255;
var displayMenu = true;
var copyCoords = "";

var region = {};
var encounters = {};
var encounterMap = {};
var weenieNames = {};
var generatorItems = {};
var currentBlockSpawns = 0;
var currentBlockSelection;
var currentBlockId = -1;
var currentCellId = null;
var currentGenerator = 0;

// dimensions of the map image we have
var imgWidth = 2041;
var imgHeight = 2041;
var imgHalfWidth = imgWidth / 2;
var imgHalfHeight = imgHeight / 2;

const imgToCells = 2040 / imgHeight;

// landblock delta in canvas space
var cdx = imgWidth / gridCount;
var cdy = imgHeight / gridCount;

// encounter delta
var edx = cdx / 8;
var edy = cdy / 8;

// zoom level to show encounter data
const Zoom_Encounters = 15.0;

// dimensions of the world map in the game
var mapWidth = 204;
var mapHeight = 204;
var mapHalfWidth = mapWidth / 2;
var mapHalfHeight = mapHeight / 2;

// ratio of map to image
var mapToImgWidth = mapWidth / imgWidth;
var mapToImgHeight = mapHeight / imgHeight;

// landblock size in game dimensions
var landblockWidth = mapWidth / gridCount;
var landblockHeight = mapHeight / gridCount;

var scale = 0.9;
var scaleMultiplier = .9;
var translatePos;

var a = imgHeight / mapHeight;
var b = imgHeight - mapHalfHeight * a;
var d = imgWidth / mapWidth;
var e = imgWidth - mapHalfWidth * d;

var xcenter = 0;
var ycenter = 0;

var locationArray = {};

var poiDict = {};
var npcDict = {};

var base_image = new Image();
var image_overlay = new Image();

const encounterColors = [
    'transparent', '#4363d8', '#f58231', '#911eb4', '#f032e6', '#ffe119', '#3cb44b', '#e6194B',
    '#fabebe', '#bcf60c', '#e6beff', '#9A6324', '#fffac8', '#808000', '#ffd8b1', '#800000',
];

function blockToGlobe(x, y) {
    var gx = Math.floor(((x - 0x7f) * 192.0 - 84.0) / 240.0);
    var gy = Math.floor(((y - 0x7f) * 192.0 - 84.0) / 240.0);

    return { x: gx, y: gy };
}

function globeToBlock(x, y) {
    var bx = Math.floor((x * 240.0 + 84.0) / 192.0 + 0x7f);
    var by = Math.floor((y * 240.0 + 84.0) / 192.0 + 0x7f);

    return { x: bx, y: by };
}

function canvasToCell(x, y) {
    var ix = x * imgToCells;
    var iy = (imgHeight - y) * imgToCells;

    var bx = ix >> 3;
    var by = iy >> 3;
    var cx = ix & 7;
    var cy = iy & 7;

    return {
        blockid: (bx << 8) | by,
        cellid: (bx << 24) | (by << 16) | (cx << 3) | cy,
        blockx: bx,
        blocky: by,
        cellx: cx,
        celly: cy
    }
}

function cellBlockToCanvas(cell) {
    var ix = (cell.blockx << 3);
    var iy = (cell.blocky << 3);
    ix /= imgToCells;
    iy = -(iy / imgToCells) + imgHeight;
    return { x: ix, y: iy }
}

function canvasToGlobe(x, y) {
    return {
        x: (x - imgHalfWidth + 0.5) * mapToImgWidth,
        y: ((imgHeight - y + 0.5) - imgHalfHeight) * mapToImgHeight
    };
}

function globeToCanvas(mx, my) {
    return {
        x: mx / mapToImgWidth + imgHalfWidth,
        y: -(my / mapToImgHeight + imgHalfHeight)
    };
}

function fitToContainer(canvas) {
    canvas.style.width = '100%';
    canvas.style.height = '100%';
    canvas.width = canvas.offsetWidth;
    canvas.height = canvas.offsetHeight;
}

function clampScale() {
    scale = Math.min(Math.max(scale, 0.2), 30.5);
}

function draw() {

    //var mobList = document.getElementById("mobList");
    //var npcList = document.getElementById("npcList");
    //var selectedMob = mobList.options[mobList.selectedIndex].value;
    //var selectedNPC = npcList.options[npcList.selectedIndex].value;
    var selectedMob = "None";
    //var selectedMob = "Ash Gromnie";
    var selectedNPC = "None";


    // clear canvas
    context.clearRect(0, 0, canvas.width, canvas.height);

    context.save();
    context.translate(translatePos.x, translatePos.y);
    context.scale(scale, scale);
    context.imageSmoothingEnabled = false;
    context.globalCompositeOperation = "source-over";

    context.drawImage(base_image, 0, 0);

    // if (selectedMob != "None") {
    //     if (image_overlay.complete) {
    //         context.drawImage(image_overlay, 0, 0);
    //     }
    //     //imageOverlay = new Image();
    //     //imageOverlay.src = 'http://mobtracker.yewsplugins.com/BigMaps/' + selectedMob + '.gif';
    //     //context.drawImage(imageOverlay, 0, 0);
    // }

    colorLandblocks(context);

    // if (selectedNPC != "None") {
    //     var npcName = selectedNPC;
    //     var npc = npcDict[npcName];

    //     for (var i = 0; i < npc.Coordinates.length; ++i) {
    //         selectedNPCCoords = npc.Coordinates[i];
    //         var splitCoords = selectedNPCCoords.split(/[\s,]+/);
    //         var y = decodeMapString(splitCoords[0]);
    //         var x = decodeMapString(splitCoords[1]);
    //         var width = 50;
    //         var Type = "WikiNPC";
    //         var Race = "";
    //         var Special = "";
    //         var isHighlighted = "";
    //         var isLandblock = "";

    //         drawPoint(context, x, y, width, Type, Race, Special, isHighlighted, isLandblock);
    //     }
    // }

    // var dPointsArrayLength = dPoints.length;

    // for (var poiName in poiDict) {
    //     var poitem = poiDict[poiName];
    //     var isHightlighted = (highlightedPointName == poitem.LocationName);
    //     var isLandblock = -1;
    //     drawPoint(context, poitem.x, poitem.y, 5, poitem.Type, poitem.Race, poitem.Special, isHightlighted, isLandblock);
    // }
    // for (i = 0; i < dPointsArrayLength; i++) {
    //     var isHightlighted = (highlightedDynPoint == i);
    //     var isLandblock = (landblockDynPoint == i);
    //     drawPoint(context, dPoints[i].x, dPoints[i].y, 5, dPoints[i].Type, dPoints[i].Race, dPoints[i].Special, isHightlighted, isLandblock);
    // }

    drawGrid(context);

    context.restore();
}

function logLocation(canvas, scale, translatePos) {
    var ax = (translatePos.x - xcenter) / scale;
    var ay = (translatePos.y - ycenter) / scale;
    var w1 = canvas.width / scale;
    var h1 = canvas.height / scale;
}

function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
        x: evt.clientX - rect.left,
        y: evt.clientY - rect.top
    };
}

function includesSubstring(x, sub) {
    return x.indexOf(sub) >= 0;
}

// function getPoints() {
//     poiDict = {};
//     var xmlhttp = new XMLHttpRequest();
//     xmlhttp.onreadystatechange = function () {
//         if (this.readyState == 4 && this.status == 200) {
//             var json = JSON.parse(this.responseText);
//             var totalItems = Object.keys(json).length;
//             for (var i = 0; i < totalItems; i++) {
//                 var x = json[i].x;
//                 var y = json[i].y;
//                 var landblock = json[i].locationString;

//                 if ("undefined" === typeof json[i].locationString) {
//                     if (includesSubstring(x, 'E')) {
//                         x = x.substring(0, x.length - 1);
//                         x = x * 1;
//                     }
//                     else {
//                         xInt = x.substring(0, x.length - 1);
//                         x = xInt * -1;
//                     }


//                     if (includesSubstring(y, 'S')) {
//                         y = y.substring(0, y.length - 1);
//                         y = y * 1;
//                     }
//                     else {
//                         yInt = y.substring(0, y.length - 1);
//                         y = yInt * -1;
//                     }
//                 }
//                 else {
//                     var locationString = json[i].locationString;

//                     // Figure out landblock coordinates
//                     var lbX = locationString.substring(0, 2);
//                     var lbY = locationString.substring(2, 4);

//                     var lbCoordX = parseInt(lbX, 16);
//                     var lbCoordY = parseInt(lbY, 16);
//                     // Convert landblock coordinates into map coordinates
//                     //var coords = coordsFromLandblock(lbCoordX, lbCoordY);
//                     var coords = blockToGlobe(lbCoordX, lbCoordY);

//                     // Figure out the landcell coordinates
//                     var tokens = locationString.split(' ');
//                     var lcx = tokens[2];
//                     var lcy = tokens[1];

//                     // Convert landcell coordinates to map offsets
//                     var offsetX = landblockWidth * ((lcx - 1) / 192);
//                     var offsetY = landblockHeight * ((lcy - 1) / 192);

//                     // Combine to get final map coordinates
//                     var cx = coords.x + offsetX;
//                     var cy = coords.y + offsetY;

//                     x = cx;
//                     y = cy;
//                 }

//                 var point = { Type: json[i].Type, Race: json[i].Race, Special: json[i].Special, LocationName: json[i].LocationName, x: x, y: y };
//                 //points.push(point);

//                 if (point.LocationName in poiDict == false) {
//                     poiDict[point.LocationName] = point;
//                 } else {
//                     console.log("Duplicate location name: " + point.LocationName);
//                 }
//             }
//             getHousingPoints();
//         }
//     };


//     xmlhttp.open("GET", "coords.json?_=" + new Date().getTime().toString(), true);
//     xmlhttp.send();
// }

// function getHousingPoints() {
//     points = new Array();
//     var xmlhttp = new XMLHttpRequest();
//     xmlhttp.onreadystatechange = function () {
//         if (this.readyState == 4 && this.status == 200) {
//             var json = JSON.parse(this.responseText);
//             var totalItems = Object.keys(json).length;
//             for (var i = 0; i < totalItems; i++) {
//                 var housingLocation = json[i].location; // format is "9.6N 36.9E" or "22.5N, 4.9E"
//                 housingLocation = housingLocation.replace(",", "");
//                 var sep = housingLocation.indexOf(" ");
//                 // Note that y is first and x is second
//                 var y = housingLocation.substring(0, sep);
//                 var x = housingLocation.slice(sep - housingLocation.length);
//                 var landblock = json[i].locationString;

//                 if (includesSubstring(x, 'E')) {
//                     x = x.substring(0, x.length - 1);
//                     x = x * 1;
//                 }
//                 else {
//                     xInt = x.substring(0, x.length - 1);
//                     x = xInt * -1;
//                 }


//                 if (includesSubstring(y, 'S')) {
//                     y = y.substring(0, y.length - 1);
//                     y = y * 1;
//                 }
//                 else {
//                     yInt = y.substring(0, y.length - 1);
//                     y = yInt * -1;
//                 }

//                 var point = { Type: "Housing", LocationName: json[i].name, x: x, y: y };
//                 point["HouseCount"] = json[i].houseCount;
//                 //point["ImgUrl"] = json["imgUrl-src"];

//                 if (point.LocationName in poiDict == false) {
//                     poiDict[point.LocationName] = point;
//                 } else {
//                     console.log("Duplicate location name: " + point.LocationName);
//                 }
//             }
//         }
//     };


//     xmlhttp.open("GET", "housing.json?_=" + new Date().getTime().toString(), true);
//     xmlhttp.send();
// }

// Convert strings to numbers, eg, 10.0W => -10.0
function decodeMapString(mstr) {
    if (includesSubstring(mstr, 'E') || includesSubstring(mstr, 'S')) {
        val = mstr.substring(0, mstr.length - 1);
        val = val * 1;
    }
    else {
        val = mstr.substring(0, mstr.length - 1);
        val = val * -1;
    }
    return val;
}

function reloadLocationArray(dPoints) {
    locationArray = {};
    var dPointsArrayLength = dPoints.length;
    for (i = 0; i < dPointsArrayLength; i++) {
        var dpt = dPoints[i];
        //var block = getLandblock(dpt.x, dpt.y);
        var block = globeToBlock(dpt.x, dpt.y);
        if (block.x in locationArray == false) {
            locationArray[block.x] = {};
        }
        if (block.y in locationArray[block.x] == false) {
            locationArray[block.x][block.y] = {};
        }
        if (dpt.Type == "Player") {
            locationArray[block.x][block.y].Players = 1;
        } else if (dpt.Type == "Landblock") {
            locationArray[block.x][block.y].Activated = 1;
        }
    }
}

function scoords(x, y) {
    return sdisp2(x).toString() + ", " + sdisp2(y).toString();
}

function sdisp2(val) {
    return Math.round(val * 100) / 100;
}

function drawPoint(context, x, y, width, Type, Race, Special, isHighlighted, isLandblock) {
    // Convert map coordinates to canvas coordinates
    var canx = d * x + e;
    var cany = a * y + b;
    circleRadius = 3 / Math.sqrt(scale);
    rectWidth = 10 / Math.sqrt(scale);

    if (Type == "Town") {
        if (document.getElementById("DisplayTown").checked) {
            town_image = new Image();
            if (Race == "Aluvian") {
                town_image.src = 'images/Map_Point_Aluv_Town.png';
            }
            else if (Race == "Sho") {
                town_image.src = 'images/Map_Point_Sho_Town.png';
            }
            else if (Race == "Gharu'ndim") {
                town_image.src = 'images/Map_Point_Gharu_Town.png';
            }
            else if (Race == "Viamontian") {
                town_image.src = 'images/castleTower.png';
            }
            else {
                town_image.src = 'images/Map_Point_Town.png';
            }
            context.drawImage(town_image, canx - rectWidth / 2, cany - rectWidth / 2, rectWidth, rectWidth);
        }
    }
    else if (Type == "Cottages" || Type == "Housing") {
        if (document.getElementById("DisplayHousing").checked) {
            context.beginPath();
            context.arc(canx, cany, circleRadius, 0, 2 * Math.PI);
            context.fillStyle = '#00FF00';
            context.fill();
            context.lineWidth = 1;
            context.strokeStyle = '#00FF00'
            context.stroke();
            context.closePath();
        }
    }

    if (isHighlighted) {
        var oldAlpha = context.globalAlpha;
        context.globalAlpha = 0.5;
        context.fillStyle = "red";
        context.fillRect(canx - rectWidth / 2, cany - rectWidth / 2, rectWidth, rectWidth);
        context.globalAlpha = oldAlpha;
    }
}

function clearSelection() {
    highlightedDynPoint = -1;
    highlightedPoint = -1;
    highlightedPointName = "";
    var coordinatesElement = document.getElementById("coords");
    coordinatesElement.innerText = "";
    //var collisionElement = document.getElementById("CollisionInfo");
    //collisionElement.innerHTML = "";
    var landblockElement = document.getElementById("lb");
    landblockElement.innerText = "";

}

function collides(x, y) {
    var isCollision = false;
    var isLandblock = false;
    //var threeSixtyView = document.getElementById("360");
    for (var poiName in poiDict) {
        var poitem = poiDict[poiName];
        var type = poitem.Type;
        // optimize by ignoring points not shown
        if (type == "Town" && !document.getElementById("DisplayTown").checked) {
            continue;
        }
        if (type == "Housing" && !document.getElementById("DisplayHousing").checked) {
            continue;
        }

        var left = poitem.x - (1 / Math.sqrt(scale)), right = poitem.x + (1 / Math.sqrt(scale));
        var top = poitem.y - (1 / Math.sqrt(scale)), bottom = poitem.y + (1 / Math.sqrt(scale));
        if (right >= x
            && left <= x
            && bottom >= y
            && top <= y) {
            isCollision = true;
            isLandblock = true;

            if (type == "Landblock") {
                landblockDynPoint = true;
            }
            else if (type == "Town" && document.getElementById("DisplayTown").checked) {
                getPointDataHTML(poitem);
            }
            else if (type == "Housing" && document.getElementById("DisplayHousing").checked) {
                getPointDataHTML(poitem);
            }
        }
    }

}

function getPointDataHTML(poitem) {
    var collisionElement = document.getElementById("CollisionInfo");
    collisionElement.innerHTML = "";
    var type = poitem.Type;
    var locationName = poitem.LocationName;
    highlightedPointName = locationName;
    landblockPointName = locationName;
    var race = poitem.Race;
    var special = poitem.Special;
    var houseCount = poitem.HouseCount;
    var description = poitem.Description;
    var html = "Type: " + type;

    if (locationName != undefined && locationName != "") {
        html += "<br />" + "Name: " + locationName;
    }
    if (race != undefined && race != "") {
        html += "<br />" + "Race: " + race;
    }
    if (special != undefined && special != "") {
        html += "<br />" + "Special: " + special;
    }

    if (houseCount != undefined && houseCount != "") {
        html += "<br />" + "Houses: " + houseCount;
    }
    if (description != undefined && description != "") {
        if (description.length > 20) {
            description = description.substring(0, 18) + "...";
        }
        html += "<br />" + "Description: " + description;

    }

    collisionElement.innerHTML = html;
}

function colorLandblocks(context) {
    if (currentBlockSpawns == 0) return;

    var canvasBlockWidth = landblockWidth * d;
    var canvasBlockHeight = landblockHeight * a;
    var oldAlpha = context.globalAlpha;
    context.globalAlpha = 0.25;

    //context.beginPath();

    for (var x = 0; x < 255; x++) {
        var bx = x * 255;
        var cx = x * cdx;
        var lbx = x * 256;
        for (var y = 0; y < 255; y++) {
            var slot = region.encounterMap[bx + y];
            var cy =  imgHeight - ((y + 1) * cdy);

            if (scale >= Zoom_Encounters) {
                var lb = lbx + y;
                var el = encounterMap[lb];

                if (lb == currentBlockId && el) {
                    //console.log('found encounters');
                    for (var ex = 0; ex < 9; ex++) {
                        for (var ey = 0; ey < 9; ey++) {
                            var idx = ex * 9 + (8 - ey);
                            //console.log(idx, el[idx], encounterColors[el[idx]]);
                            context.globalAlpha = 0.5;
                            context.fillStyle = encounterColors[el[idx]];
                            // these go bottom-up
                            context.fillRect(
                                cx - (edx / 2) + ex * edx,
                                cy - (edy / 2) + ey * edy,
                                edx,
                                edy);
                        }
                    }
                    continue;
                }
            }

            if (slot == currentBlockSpawns) {
                context.globalAlpha = 0.25;
                context.fillStyle = "cyan";
                context.fillRect(cx, cy, canvasBlockWidth, canvasBlockHeight);
            }
        }
    }

    //context.closePath();
    //context.fill();
    context.globalAlpha = oldAlpha;
}

function drawEncounterGrid(context) {
    if (!currentCellId) return;

    var linewidth = 0.1;
    var oldAlpha = context.globalAlpha;
    context.globalAlpha = 0.5;
    context.strokeStyle = "gray";
    context.lineWidth = 0.1;

    context.beginPath();

    var cc = cellBlockToCanvas(currentCellId);
    //console.log(currentCellId, cc);

    const hedx = edx / 2;
    const hedy = edy / 2;
    var dx = cdx + edx - hedx;
    var dy = cdy + edy - hedy;

    for (var ex = 0; ex < 10; ex++) {
        var mex = ex * edx - hedx;
        context.moveTo(cc.x + mex, cc.y + hedy);
        context.lineTo(cc.x + mex, cc.y - dy);
    }

    for (var ey = 0; ey < 10; ey++) {
        var mey = ey * edy - hedy;
        context.moveTo(cc.x - hedx, cc.y - mey);
        context.lineTo(cc.x + dx, cc.y - mey);
    }

    context.closePath();
    context.stroke();
    context.globalAlpha = oldAlpha;
}

function drawGrid(context) {
    var oldAlpha = context.globalAlpha;
    context.globalAlpha = 0.5;
    context.strokeStyle = "black";

    context.beginPath();

    for (var xf = 0; xf < 256; xf++) {
        var mx = xf * cdx;
        context.moveTo(mx, 0);
        context.lineTo(mx, imgHeight);
    }

    for (var yf = 0; yf < 256; yf++) {
        var my = yf * cdy;
        context.moveTo(0, my);
        context.lineTo(imgWidth, my);
    }

    context.closePath();
    context.stroke();
    context.globalAlpha = oldAlpha;

    if (scale >= Zoom_Encounters) {
        drawEncounterGrid(context);
    }
}

function showMenu() {
    if (displayMenu) {
        displayMenu = false;
        document.getElementById("derethMenu").innerText = "Show Menu";
        document.getElementById("menuItems").style.display = 'none';
        //document.getElementById("iframeHolder").style.display = 'none';
    }
    else {
        displayMenu = true;
        document.getElementById("derethMenu").innerText = "Hide Menu";
        document.getElementById("menuItems").style.display = 'block';
        //document.getElementById("iframeHolder").style.display = 'block';
    }
}

function showLandblockClicked() {
    if (document.getElementById("DisplayLandblockGrid").checked) {
        document.getElementById("mapAlpha").value = 5;
    }
    else {
        document.getElementById("mapAlpha").value = 0;
    }
}

function displayResize() {
    fitToContainer(document.getElementById("mapCanvas"));
    draw();
}

function copyToClipboard() {
    var text = copyCoords;
    window.prompt("Copy to clipboard: Ctrl+C, Enter", text);
}

(function ($) {
    $.fn.disableSelection = function () {
        return this.each(function () {
            if (typeof this.onselectstart != 'undefined') {
                this.onselectstart = function () { return false; };
            } else if (typeof this.style.MozUserSelect != 'undefined') {
                this.style.MozUserSelect = 'none';
            } else {
                this.onmousedown = function () { return false; };
            }
        });
    };
})(jQuery);

function init(getName, getGenerated) {
//window.onload = function () {

    function updateGenerators(sel) {
        var gl = $('#generators');
        gl.children().remove();

        $.each(region.encounters, function(key, val) {
            if (val.key == sel) {
                $.each(val.value, function(i, v) {
                    //var opt = $('<option></option>').attr('value', v).text(v);
                    var opt = $('<li></li>').attr('value', v).text(v);
                    getName(v).then(function(weenieName) {
                        opt.text(weenieName);
                    });
                    opt.click(function (evt) {
                        currentGenerator = i;
                        $('#generators .active').toggleClass('active');
                        $(this).toggleClass('active');
                    });
                    gl.append(opt);
                })
            }
        });
    }

    $(window).resize(function() {
        displayResize();
    });

    $('#spawns').change(function() {
        var val = parseInt(this.value);
        if (val != currentBlockSpawns) {
            currentBlockSpawns = val;
            region.encounterMap[currentBlockSelection] = val;
            updateGenerators(currentBlockSpawns);
            draw();
        }
    });

    canvas = document.getElementById("mapCanvas");
    xcenter = canvas.offsetWidth;
    ycenter = canvas.offsetHeight;
    context = canvas.getContext("2d");

    base_image.onload = function() {
        draw();
    };
    base_image.src = '../ui/res/highres.png';

    // var imgload = new Image();
    // imgload.onload = function() {
    //     var tc = document.createElement('canvas');
    //     tc.width = imgload.width;
    //     tc.height = imgload.height;

    //     var tctx = tc.getContext("2d");
    //     tctx.drawImage(imgload, 0, 0);

    //     var id = tctx.getImageData(0, 0, tc.width, tc.height);
    //     for (var i = 0; i < id.data.length; i += 4) {
    //         var r = id.data[i];
    //         var g = id.data[i+1];
    //         var b = id.data[i+2];
    //         if (r == 0 && g == 0 && b == 0) {
    //             id.data[i+3] = 0;
    //         }
    //     }
    //     tctx.putImageData(id, 0, 0);
    //     image_overlay.src = tc.toDataURL('image/png');
    // };
    // imgload.src = 'http://mobtracker.yewsplugins.com/BigMaps/Diamond Golem.gif';

    fitToContainer(canvas);

    //var coordinates = coordsFromLandblock(00, 00);
    var coordinates = blockToGlobe(0, 0);

    translatePos = {
        x: canvas.width / 2,
        y: canvas.height / 2
    };
    var absoluteOffset = {
        x: canvas.width / 2,
        y: canvas.height / 2
    };
    // Viewport offset in relative (screen) numbers
    translatePos.x = 0;
    translatePos.y = 0;

    // Viewport offset in absolute (canvas) numbers
    absoluteOffset.x = 0;
    absoluteOffset.y = 0;


    var startDragOffset = {};
    var mouseDown = false;

    // add event listeners to handle screen drag
    canvas.addEventListener("mousedown", function (evt) {
        mouseDown = true;
        $('*').disableSelection();
        startDragOffset.x = evt.clientX - translatePos.x;
        startDragOffset.y = evt.clientY - translatePos.y;
    });

    canvas.addEventListener("mouseup", function (evt) {
        mouseDown = false;

        // Get mouse position inside canvas screen (removes client side offsets)
        var mpos = getMousePos(canvas, evt)

        // Convert to canvas coordinates
        var canco = {
            x: (mpos.x - translatePos.x) / scale,
            y: (mpos.y - translatePos.y) / scale
        };

        var xy = canvasToGlobe(canco.x, canco.y);
        var cell = canvasToCell(canco.x, canco.y);

        if (evt.button == 0) {
            // left click
            currentCellId = cell;
            clearSelection();
            displayCoord(xy.x, xy.y);
            //collides(xy.x, xy.y);
            displayLandblock(xy.x, xy.y);

        } else if (evt.button == 2) {
            // right click
            var el = encounterMap[currentCellId.blockid];
            if (!el) {
                el = Array.apply(null, Array(81)).map((v, i) => 0);
                var enc = { 'key' : currentCellId.blockid, 'value': el };
                encounters.push(enc);
                encounterMap[currentCellId.blockid] = el;
                
            }
            var eli = -1;

            // normally things are 8x8, but this is 9x9
            var ix = (canco.x + edx / 2) * imgToCells - (currentCellId.blockx << 3) | 0;
            var iy = (imgHeight - canco.y + edy / 2) * imgToCells - (currentCellId.blocky << 3) | 0;

            eli = ix * 9 + iy;
            console.log(el, eli);

            if (eli >= 0 && eli < 81)
                el[eli] = currentGenerator;
        }

        draw();
    });

    function displayLandblock(mx, my) {
        block = globeToBlock(mx, my);
        //console.log(block, mx, my);
        currentBlockId = block.x * 256 + block.y;
        document.getElementById("lb").innerText = "Landblock: " + block.x + " (0x" + block.x.toString(16) + ") " + block.y + " (0x" + block.y.toString(16) + ")";

        if (region.encounterMap) {
            currentBlockSelection = block.x * 255 + block.y;
            var sel = region.encounterMap[currentBlockSelection];
            var dd = $('#spawns');

            currentBlockSpawns = sel;

            dd.val(sel);
            $('#groupName').val(region.encounters[sel].desc);
            updateGenerators(sel);
        }
    }
    function displayCoord(x, y) {
        var multiplier = Math.pow(10, 1 || 0);
        var roundedX = Math.round(x * multiplier) / multiplier;
        var roundedY = Math.round(y * multiplier) / multiplier;
        var xWithCompass = "0.0";
        var yWithCompass = "0.0";
        if (roundedX > 0) {
            xWithCompass = Math.abs(roundedX).toString() + "E";
        }
        else if (roundedX < 0) {
            xWithCompass = Math.abs(roundedX).toString() + "W";
        }
        if (roundedY < 0) {
            yWithCompass = Math.abs(roundedY).toString() + "S";
        }
        else if (roundedY > 0) {
            yWithCompass = Math.abs(roundedY).toString() + "N";
        }

        document.getElementById("coords").innerText = "Coordinates: " + yWithCompass + ", " + xWithCompass;
        copyCoords = yWithCompass + ", " + xWithCompass;

    }
    canvas.addEventListener("wheel", function (evt) {
        if (evt.wheelDelta == 0) return;

        absoluteOffset.x = (translatePos.x - evt.clientX) / scale;
        absoluteOffset.y = (translatePos.y - evt.clientY) / scale;

        if (evt.wheelDelta > 0) {
            scale /= scaleMultiplier;
        } else {
            scale *= scaleMultiplier;
        }
        clampScale();

        translatePos.x = (scale * absoluteOffset.x) + evt.clientX;
        translatePos.y = (scale * absoluteOffset.y) + evt.clientY;

        draw();
    });

    canvas.addEventListener("mouseover", function (evt) {
        mouseDown = false;
    });

    canvas.addEventListener("mouseout", function (evt) {
        mouseDown = false;
    });

    canvas.addEventListener("mousemove", function (evt) {
        if (mouseDown) {
            translatePos.x = evt.clientX - startDragOffset.x;
            translatePos.y = evt.clientY - startDragOffset.y;
            draw();
        }
    });

    canvas.addEventListener('dblclick', function (evt) {
        absoluteOffset.x = (translatePos.x - xcenter) / scale;
        absoluteOffset.y = (translatePos.y - ycenter) / scale;

        scale /= scaleMultiplier;

        translatePos.x = (scale * absoluteOffset.x) + xcenter;
        translatePos.y = (scale * absoluteOffset.y) + ycenter;

        logLocation(canvas, scale, translatePos);

        draw();
        mouseDown = false;
    });

    //getPoints();
    //setInterval(function () { draw(); }, 1500);
};

function setRegion(data) {
    region = data;

    var dd = $('#spawns');

    dd.children().remove();
    dd.append($('<option value="0">None</option>'));

    $.each(region.encounters, function(key, val) {
        dd.append($('<option></option>').attr('value', val.key).text(val.desc || val.key).data(val));
    });

    draw();
}

function setEncounters(data) {
    encounters = data;
    data.forEach((val, idx, arr) => encounterMap[val.key] = val.value);
    draw();
}

module.exports = exports = {
    init: init,
    blockToGlobe: blockToGlobe,
    globeToBlock: globeToBlock,
    canvasToGlobe: canvasToGlobe,

    setRegion: setRegion,
    getRegion: () => region,

    setEncounters: setEncounters,
    getEncounters: () => encounters
}