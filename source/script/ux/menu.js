"use strict";

const {
    app,
    Menu
} = require('electron');

module.exports = exports = (clicks) => {
    clicks = clicks || {};
    var template = [
        {
            label: 'File',
            submenu: [
                {
                    label: 'Open',
                    click: clicks['Open']
                },
                {
                    label: 'Save',
                    click: clicks['Save']
                },
                {
                    label: 'Save As...',
                    click: clicks['SaveAs']
                },
                {   type: 'separator'   },
                {   role: 'quit'        }
            ]
        },
        {
            label: 'Edit',
            submenu: [
                {   role: 'undo'        },
                {   role: 'redo'        },
                {   type: 'separator'   },
                {   role: 'cut'         },
                {   role: 'copy'        },
                {   role: 'paste'       },
                {   role: 'selectall'   }
            ]
        },
        {
            role: 'help',
            submenu: [
                {
                    label: 'About',
                    click: clicks['About']
                },
                {   role: 'reload'          },
                {   role: 'toggledevtools'  }
            ]
        }
    ];
    this.menu = Menu.buildFromTemplate(template);
    Menu.setApplicationMenu(this.menu);
};