"use strict";

const {
	ipcRenderer,
	remote
} = require('electron');

const {
    dialog
} = require('electron').remote;

const fs = require('fs');
const path = require('path');

const $route = require('../util/route.js');

let controllers = {};
let activeController = null;

let loadedData = {};
let dataPath = null;

function loadController(which) {
	$route(which, 'main', 'view/').then(() => {
		which = which.substr(1);
		let controller = null;
		if (controllers.hasOwnProperty(which)) {
			controller = controllers[which];
		} else {
			controller = require('../controller/' + which + '.js');
			controllers[which] = controller;
			controller.loadedData = loadedData;
			controller.init();
		}
		if (activeController) {
			// stop it
			activeController.deactivate();
		}
		activeController = controller;
		controller.activate();
	});
}

$(document).ready(function () {
	dataPath = remote.getGlobal('dataPath');
	if (!dataPath) {
		selectDataPath();
	}

    var start = location.hash || $('main').attr('default');
	loadController(start);
    //$route(start, 'main', 'view/');
	$(window).on('hashchange', function(evt) {
		loadController(location.hash);
		//$route(location.hash, 'main', 'view/');
	});
});

ipcRenderer.on('menu-file-open', selectDataPath);
ipcRenderer.on('menu-file-save', saveDataFiles);

function loadDataFile(file, type) {
	var dataSrc = loadedData;
	var filePath = dataPath;
	if (type !== undefined) {
		filePath = path.join(filePath, type);
		if (!(type in dataSrc)) dataSrc[type] = {};
		dataSrc = dataSrc[type];
	}

	var dataObj = dataSrc[file];
	if (dataObj) {
		return new Promise((res, rej) => {
			res(dataObj);
			return dataObj;
		});
	} else {
		return new Promise((res, rej) => {
			fs.readFile(path.join(filePath, file), function(e, data) {
				if (e) {
					rej(e);
				} else {
					var obj = JSON.parse(data);
					dataSrc[file] = obj;
					res(obj);
				}
			});
		});
	}
}

function selectDataPath(event) {
    var paths = dialog.showOpenDialog(
        {
            filters : [
                { name : 'JSON Files', extensions: [ 'json' ] },
                { name : 'All Files', extensions: [ '*' ] }
            ],
            properties : [ 'openDirectory' ]
		});

	if (paths) {
		dataPath = paths[0];
		ipcRenderer.send('updateDataPath', dataPath);
	}
}

function saveDataFiles(event) {
	var dataSrc = loadedData;

	for (const type in dataSrc) {
		var typeData = dataSrc[type];
		var filePath = path.join(dataPath, type);

		for (const file in typeData) {
			fs.writeFileSync(path.join(filePath, file), JSON.stringify(typeData[file]));
		}
	}
}