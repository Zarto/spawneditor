
const { google } = require('googleapis');
const sheets = google.sheets('v4');

module.exports = exports = {
    load: (spreadSheetId, apiKey) => {
        sheetId = spreadSheetId;

        sheets.spreadsheets.values.batchGet(
            {
                spreadsheetId: spreadSheetId,
                key: apiKey,
                ranges: [
                    'WCID List!A:B',
                    'Generator List!D:E'
                ]
            },
            (err, result) => {
                if (err) {
                    console.error(err);
                } else {
                    // stuff
                    console.log(result);
                    // result.data.valueRanges[0].values

                    $.each(result.data.valueRanges[0].values, function(idx, item) {
                        if (idx > 0) {
                            if (item && item.length > 1) {
                                weenieNames[item[0]] = item[1];
                            }
                        }
                    });

                    $.each(result.data.valueRanges[1].values, function(idx, item) {
                        if (idx > 0) {
                            if (item && item.length > 2) {
                                generatorItems[item[0]] = item[2].split(',');
                                //weenieNames[item[0]] = item[1];
                            }
                        }
                    });
                }
            }
        );
    },

    getName: (weenieId) => {
        return weenieNames[weenieId] || weenieId;
    },

    getGeneratorList: (weenieId) => {
        return generatorItems[weenieId];
    }
};

var sheetId;
var weenieNames = {};
var generatorItems = {};