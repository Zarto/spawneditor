"use strict";

const {
    app,
//    BrowserWindow,
//    dialog,
    ipcMain
} = require('electron');

//const fs = require('fs');

const menu = require('./ux/menu');
const window = require('./ux/window');

const appName = "Spawn Editor";
//const dataPath = app.getPath('userData');

//console.log(dataPath);

let mainWindow;

if (app.makeSingleInstance(
        (args, working) => {
            if (mainWindow) {
                if (mainWindow.isMinimized()) mainWindow.restore();
                mainWindow.focus();
            }
        }))
{
    app.quit();
}

app.on('window-all-closed', () => { app.quit(); });

app.on('ready', () => {
    mainWindow = new window(appName);
    menu({
        'Open' : function() {
            mainWindow.bw.webContents.send('menu-file-open');
        },
        'Save' : function() {
            mainWindow.bw.webContents.send('menu-file-save');
        },
        'SaveAs' : function() {
            mainWindow.bw.webContents.send('menu-file-save-as');
        }
    });

    mainWindow.load('../ui/index.html');
});

ipcMain.on('updateDataPath', (event, data) => global.dataPath = data);


//https://blog.avocode.com/blog/4-must-know-tips-for-building-cross-platform-electron-apps
//https://github.com/sindresorhus/awesome-electron