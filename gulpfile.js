"use strict";

// Sass configuration
var gulp = require('gulp');
var sass = require('gulp-sass');

var cleanCSS = require('gulp-clean-css');
var uglify = require('gulp-uglify');

//var rename = require('gulp-rename');
//var foreach = require('gulp-foreach');
var pump = require('pump');
var del = require('del');

var path = require('path');
//var open = require('open');

var paths = {
    dest : ".dist/",
    build : ".tmp/",
    style : 'source/style/',
    script: 'source/script/',
    content: 'source/content/',
    data: 'data/'
};

var outputs = {
    dest : `${paths.dest}`,
    build : `${paths.tmp}`,
    style : `${paths.dest}ui/style`,
    script: `${paths.dest}`,
    scriptlib: `${paths.dest}lib`,
    content: `${paths.dest}ui`,
    data: `${paths.dest}`
}

var cleanPaths = [
    paths.dest,
    paths.build
];

var scriptlibs = [
    "node_modules/jquery/dist/jquery.min.js",
    "node_modules/jquery.loadtemplate/dist/jquery.loadTemplate.min.js"
];

var styleassets = [
    'node_modules/font-awesome/fonts/*'
];

var proc = require('child_process');
function runcmd(cmd, args, cb, pass){
    pass = typeof pass !== 'undefined' ? pass : true;
    let command = proc.spawn(cmd, args);
    command.stdout.pipe(process.stdout);
    command.stderr.pipe(process.stderr);
    if (cb) {
        command.on('close', function(code) {
            if (!pass) {
                if (code !== 0) { cb(code); }
                else { cb(); }
            }
        });
    }
    command.pipe = function(to) {
        return command.stdin.pipe(to);
    }
    command.end = function() { command.stdin.end(); }
    return command;
}

gulp.task('clean', function() {
    return del(cleanPaths);
});

gulp.task('cssassets', function (cb) {
    pump([
        gulp.src(styleassets),
        gulp.dest(outputs.style)
    ], cb);
});

gulp.task('scriptlibs', function(cb) {
    pump([
        gulp.src(scriptlibs, { base: '' }),
        gulp.dest(outputs.scriptlib)
    ], cb);
});

gulp.task('content', function (cb) {
    pump([
        gulp.src(paths.content + '**/*', { base: '' }),
        gulp.dest(outputs.content)
    ], cb);
});

gulp.task('css', gulp.series('cssassets', function (cb) {
    pump([
        gulp.src(paths.style + '*.scss'),
        sass({ includePaths: [
                require('bourbon').includePaths,
                require('bourbon-neat').includePaths,
                require('node-normalize-scss').includePaths,
                'node_modules/font-awesome/scss'
            ] }),
        //cleanCSS(),
        gulp.dest(outputs.style)
    ], cb);
}));

gulp.task('script', gulp.series('scriptlibs', function (cb) {
    pump([
        gulp.src(paths.script + '**/*.js', { base: '' }),
        //uglify(),
        gulp.dest(outputs.script)
    ], cb);
}));

gulp.task('assets', gulp.parallel(['cssassets', 'scriptlibs', 'content']));
gulp.task('build', gulp.parallel(['assets', 'css', 'script']));
gulp.task('rebuild', gulp.parallel(['clean', 'build']));

gulp.task('open', function () {
    open(paths.output + 'index.html');
});

gulp.task('default', gulp.series('build'));

gulp.task('run', gulp.series('build', function(cb) {
    runcmd('node_modules/electron/dist/electron', [ `${outputs.script}main.js` ]);
    cb();
}));

gulp.task('package', gulp.series('rebuild', function(cb) {
    pump([
        gulp.src("package.json"), gulp.dest(outputs.dest),
        runcmd('electron-packager', [ '.dist', 'SpawnEditor', '--platform win32', '--arch x64' ])
    ], cb);
}));
